# Various scripts

Collection of various scripts that have no special topics.

<br>

### Testing transcription on raspberry pi

Enable cross-building if you don't want to build the Container on the RasPi itself:

```bash
sudo podman run --security-opt label=disable --rm --privileged multiarch/qemu-user-static --reset -p yes
```

Build container:

```bash
podman build --cgroup-manager=cgroupfs -f Scribosermo/extras/misc/ctcdecoder_armv7/Containerfile_Raspbian -t scribosermo_raspbian
```

Run script: \
(For performance tests, restart the pi every time, to prevent caching speedups)

```bash
podman run --privileged --rm --network host -it \
  --volume "$(pwd)"/Scribosermo/extras/exporting/:/Scribosermo/extras/exporting/:ro \
  --volume "$(pwd)"/Scribosermo/data/:/Scribosermo/data/:ro \
  --volume "$(pwd)"/checkpoints/:/checkpoints/:ro \
  --volume "$(pwd)"/data_prepared/texts/:/data_prepared/texts/:ro \
  scribosermo_raspbian

python3 /Scribosermo/extras/exporting/testing_tflite.py
```
